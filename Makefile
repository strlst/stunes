OPTIONS:=
OPTIONS+= -l m

stunes: stunes.c
	gcc -Wall -std=c99 -pedantic -g stunes.c st.c -o stunes $(OPTIONS)

clean:
	rm -rf stunes
