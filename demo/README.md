SINE - predecessor of stunes

this is merely a proof of concept, try it by typing `./stunes | aplay`

it was a minor experiment to satisfy my desire of playing sounds by piping random data into other programs/files, the result however seems to have potential

a crazy sort of sound is playing, but hopefully you'll notice that this sound is composed of some interlapping waves multiplied by some frequency, the perpetuality of the sound is a dead giveaway. the melody has its own kind of rhythm, what piqued my interest were the kick-like sounds being produced by the spikes in the tanf(...) function, almost forming a baseline for the crazy and violent sine/tan mix that could be said to form the main melody. what if we separate each of the factors into their own channels and give the user the ability to freely define multiple channels, each playing a melody by their own set of rules? 
