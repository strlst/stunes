#include <stdio.h>
#include <unistd.h>

#include "math.h"

#define PI 3.14159265358979323846

int main(int argc, char **argv) {
    /* CHANGE THESE */
    int range = 1000000;
    int freq = 3000;
    float lewave = 2*(PI/freq);
    int step = 2;
    if (step % 2 != 0) step++;

    while (1) {
        printf("%f\n", range * sinf(((step+=2)%freq) * lewave) * tanf((step++) * lewave));
        sleep(0.5);
    }
    return 0;
}
