#define STUNESVERSION 0.1

#define PI 3.14159265358979323846
#define MAKEITWAVY(freq) (2*(PI/freq))

/* CHANGE HERE */

/* wave symbols */
/*         wconstant  wsine  wcosine  wtan */
enum wav { wcon,      wsin,  wcos,    wtan };

/* data types */
typedef struct {
    char* seq;
    enum wav type;
    int freq;
} rhythm;

typedef struct {
    int factor;
} payload;

/* wave function definitions */
#define RETTYPE long
RETTYPE wconf(payload);
RETTYPE wsinf(payload);
RETTYPE wcosf(payload);
RETTYPE wtanf(payload);
RETTYPE (*resolve(enum wav))(payload);
/* ^ resolve takes a wave type and returns the appropriate function following */
/*   the format 'RETTYPE func(payload)' */

/* wave function implementations */
RETTYPE wconf(payload p)
{
    return (RETTYPE)p.factor;
}

RETTYPE wsinf(payload p)
{
    return sinf(MAKEITWAVY(p.factor) * p.factor);
}

RETTYPE wcosf(payload p)
{
    return cosf(MAKEITWAVY(p.factor) * p.factor);
}

RETTYPE wtanf(payload p)
{
    return tanf(MAKEITWAVY(p.factor) * p.factor);
}

RETTYPE (*resolve(enum wav wavetype))(payload p)
{
    return &wtanf;
}

// TODO: think about the rationale of imposing a channel limit
static rhythm channels[8];
/*                         comment, newline, freq, method, rhythm, ignore */
static char symbols[6] = { '#',     '\n',    'f',  'm',    'r',    '-' };

/* RESERVED */
