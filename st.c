#include <stdio.h>

/* function implementations */
/* checks char against all elements of a char array */
/*   returns 1 on successful match, 0 otherwise */
int in_array(char* array, char c) {
    char* arrayp = array;

    while (*arrayp != '\0') {
        if (*arrayp == c) return 1;
        arrayp++;
    }

    return 0;
}

/* probes line to check whether it contains a specific character or not */
/*   returns matched character on success, -1 otherwise */
char probe_line(FILE* in, char* array, long* read) {
    long offset = ftell(in);
    char ret = -1;

    char c;
    while (!feof(in) && c != '\n') {
        c = getc(in);
        if (in_array(array, c)) {
            ret = c;
            break;
        }
        if (c != ' ') break;
    }

    *read = ftell(in);
    fseek(in, offset, SEEK_SET);
    //printf("%li off %li read %c ret\n", offset, *read, ret);

    return ret;
}

int seek_char(FILE* in, char s) {
    long offset = ftell(in);
    char c = getc(in);
    int chars = 1;
    while(!feof(in) && c != s) {
        c = getc(in); ++chars;
    }
    fseek(in, offset, SEEK_SET);
    //printf("%li off %i chars\n", offset, chars);

    return chars;
}

void skip(FILE* in, char s) {
    char c = getc(in);
    while(!feof(in) && c != s) {
        c = getc(in);
    }
}
